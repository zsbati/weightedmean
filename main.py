# Enter your code here. Read input from STDIN. Print output to STDOUT
n = int(input().strip())
string_numbers = input().strip()
numbers = string_numbers.split(" ")
nums = []
nums = [int(stri) for stri in numbers]

string_weights = input().strip()
weights = string_weights.split(" ")
wts = []
wts = [int(stri) for stri in weights]

weighted_sum = 0; 
weights_sum = 0     
for i in range(n):
  weighted_sum += nums[i]*wts[i]
  weights_sum += wts[i]


weighted_mean = round((float(weighted_sum))/(float(weights_sum)), 1)

print(weighted_mean)
